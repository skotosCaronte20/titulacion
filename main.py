import uuid

from flask import Flask, request, render_template
from src.deal.User_deal import User_deal
from flask_bootstrap import Bootstrap
import form
from werkzeug.utils import secure_filename
import os
import json


app = Flask(__name__)
PROJECT_HOME = os.path.dirname(os.path.realpath(__file__))
UPLOAD_FOLDER = '{}/static/horarios'.format(PROJECT_HOME)
UPLOAD_PHOTOS = '{}/static/img'.format(PROJECT_HOME)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['UPLOAD_PHOTOS'] = UPLOAD_PHOTOS


@app.route("/", methods=['GET'])
def index():
	return render_template("layautl.html")

@app.route("/home", methods=['GET'])
def home():
	return render_template("home.html")


@app.route("/add_employee", methods=['GET','POST'])
def add_emloyee():
	if request.method == 'POST':
		res = request.form.to_dict(flat=False)
		img =request.files['archivosubido']
		img_name = secure_filename(img.filename)
		id = ''
		mat = res['NoEmpleado']
		for i in mat:
			id = i
		img_name ='{}.jpg'.format(id)
		create_new_folder(app.config['UPLOAD_PHOTOS'])
		saved_path = os.path.join(app.config['UPLOAD_PHOTOS'], img_name)
		img.save(saved_path)
		return User_deal().registerDeal_employe(res)
	elif request.method =='GET':
		return render_template('AgregarEmpleado.html')


def create_new_folder(local_dir):
	newpath = local_dir
	if not os.path.exists(newpath):
		os.makedirs(newpath)
	return newpath

@app.route("/add_schedule", methods=['GET','POST'])
def add_schedule():
	if request.method == 'POST':
		res = request.files['archivosubido']
		img_name = secure_filename(res.filename)
		create_new_folder(app.config['UPLOAD_FOLDER'])
		saved_path = os.path.join(app.config['UPLOAD_FOLDER'], img_name)
		res.save(saved_path)
		return User_deal().get_shedule(img_name)

	elif request.method =='GET':
		return render_template('AgregarHorario.html')

@app.route("/show_assistance", methods=['GET','POST'])
def show_assistance():
	if request.method=='GET':
		return render_template("ShowAssistance.html",list=User_deal().get_names())
	elif request.method =='POST':
		res = request.form.to_dict(flat=False)
		print(res)
		return render_template("ShowAssistance.html", message='Usuario encontrado {}'.format(res))


@app.route("/show_employee", methods=['GET', 'POST'])
def show_employee():
	if request.method == 'POST':
		res = request.form.to_dict(flat=False)
		return User_deal().findDeal_employee(res)
	elif request.method =='GET':
		return render_template('verEmpleado.html')

@app.route("/edit_employe/<string:id>",methods=['GET'])
def edit_employe(id):
	print(id)
	if request.method == 'GET':
		res = User_deal().find_edit_employe(id)
		print(res)
		return  render_template('update.html',res = res)

@app.route("/update_employe",methods=['GET','POST'])
def update_employe():
	if request.method == 'POST':
		res = request.form.to_dict(flat=False)
		img =request.files['archivosubido']
		img_name = secure_filename(img.filename)
		id = ''
		mat = res['NoEmpleado']
		for i in mat:
			id = i
		img_name ='{}.jpg'.format(id)
		create_new_folder(app.config['UPLOAD_PHOTOS'])
		saved_path = os.path.join(app.config['UPLOAD_PHOTOS'], img_name)
		img.save(saved_path)
		return User_deal().update_registers(res)
	elif request.method =='GET':
		return render_template('AgregarEmpleado.html')

@app.route("/del_employe/<string:id>", methods=['GET','POST'])
def del_employe(id):
	if request.method=='GET':
		res = User_deal().del_registers(id)
		return render_template('verEmpleado.html',delete = res)


if __name__ == ("__main__"):
	app.run(debug=True)
