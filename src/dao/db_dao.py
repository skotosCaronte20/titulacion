import psycopg2


class DbDAO:
	def connect(self):
		self.is_not_used()
		conn = None
		try:
			conn = psycopg2.connect(host="127.0.0.1", port=5432 ,database="titulacionDB", user="postgres", password="lalolosla")
			cur = conn.cursor()
			cur.execute('SELECT version()')
			db_version = cur.fetchone()
			#print(db_version)
			cur.close()
		except (Exception, psycopg2.DatabaseError) as error:
			print(error)
		finally:
			if conn is not None:
				return conn

	def is_not_used(self):
		pass
