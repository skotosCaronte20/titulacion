from src.dao.db_dao import DbDAO
import psycopg2


class UserDao:

	def register_user(self, data):
		self.is_not_used()
		sql = """SELECT add_usr(%s, %s, %s, %s, %s, %s)"""
		conn = DbDAO().connect()
		identifier = False
		try:
			cur = conn.cursor()
			print(data)
			cur.execute(sql, (data[3], data[0], data[1], data[2], data[4], data[5]))
			identifier = cur.fetchone()[0]
			print(identifier)
			conn.commit()
			cur.close()
		except (Exception, psycopg2.DatabaseError) as error:
			print(error)
		finally:
			if conn is not None:
				conn.close()
				print('Database connection closed.')

		return identifier

	def get_users(self):
		self.is_not_used()
		sql = "SELECT name FROM usr"
		conn = DbDAO().connect()
		identifier = False
		lista = []
		try:
			cur = conn.cursor()
			cur.execute(sql)
			identifier = cur.fetchall()
			for curso in identifier:
				lista.append(curso[0])
			print(lista)
			conn.commit()
			cur.close()
		except (Exception, psycopg2.DatabaseError) as error:
			print(error)
		finally:
			if conn is not None:
				conn.close()
				print('Database connection closed.')

		return lista

	def findDao_user(self, data):
		code = data
		self.is_not_used()
		sql = """SELECT row_to_json(row(get_usr(%s)))"""
		conn = DbDAO().connect()
		identifier = False
		try:
			cur = conn.cursor()
			cur.execute(sql, data['NoEmpleado'])
			identifier = cur.fetchone()[0]
			identifier = identifier['f1']
			"""
			resu = identifier.split(",")
			for i in resu:
				i = i.replace('(','').replace(')','')
				resu+=i
			print(resu)
			
			"""
			conn.commit()
			cur.close()
		except (Exception, psycopg2.DatabaseError) as error:
			print(error)
		finally:
			if conn is not None:
				conn.close()
				print('Database connection closed.')

		return identifier

	def validate_usr(self, data):
		self.is_not_used()
		sql = "SELECT exists(SELECT * FROM usr WHERE name='%s')" % data
		conn = DbDAO().connect()
		identifier = False
		try:
			cur = conn.cursor()
			cur.execute(sql, (data))
			identifier = cur.fetchone()[0]
			# print("Estado ",data,' ',identifier)
			conn.commit()
			cur.close()
		except (Exception, psycopg2.DatabaseError) as error:
			print(error)
		finally:
			if conn is not None:
				conn.close()
				print('Database connection closed.')
		return identifier

	def register_schedule(self, data):
		self.is_not_used()
		sql = """SELECT add_schedule(%s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s,%s)"""
		conn = DbDAO().connect()
		identifier = False

		try:
			cur = conn.cursor()
			cur.execute(sql, [
			data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10],
			data[11], data[12]])
			identifier = cur.fetchone()[0]
			conn.commit()
			cur.close()
		except (Exception, psycopg2.DatabaseError) as error:
			print(error)
		finally:
			if conn is not None:
				conn.close()
				print('Database connection closed.')
		return identifier


	def edit_employe(self, data):
		self.is_not_used()

	def find_edit_dao_user(self, data):

		sql = """SELECT row_to_json(row(get_usr(%s)))"""
		print("estoy en dao",data)
		conn = DbDAO().connect()
		identifier = False
		try:
			cur = conn.cursor()
			cur.execute(sql, data)
			identifier = cur.fetchone()[0]

			identifier = identifier['f1']

			conn.commit()
			cur.close()
		except (Exception, psycopg2.DatabaseError) as error:
			print(error)
		finally:
			if conn is not None:
				conn.close()
				print('Database connection closed.')

		return identifier

	def register_update(self,data):
		self.is_not_used()
		print(data)
		print("Estoy en register update")
		sql = """SELECT update_employe(%s,%s,%s,%s,%s,%s)"""
		#sql = """UPDATE usr SET name = %s, firstname = %s, lastname = %s, typeemployee = %s, type= %s WHERE code = %s"""
		conn = DbDAO().connect()
		identifier = False
		try:
			cur = conn.cursor()
			print(data)
			cur.execute(sql, (data[0], data[1], data[2], data[4], data[5],data[3]))
			identifier = cur.fetchone()[0]
			conn.commit()
			cur.close()
		except (Exception, psycopg2.DatabaseError) as error:
			print(error)
		finally:
			if conn is not None:
				conn.close()
				print('Database connection closed.')

		return identifier

	def del_user_dao(self,data):
		self.is_not_used()
		sql = "SELECT del_register(%s)"
		conn = DbDAO().connect()
		identifier = False
		try:
			cur = conn.cursor()
			cur.execute(sql, [data])
			identifier = cur.fetchone()[0]
			conn.commit()
			cur.close()
		except (Exception, psycopg2.DatabaseError) as error:
			print(error)
		finally:
			if conn is not None:
				conn.close()
				print('Database connection closed.')

		return identifier


	def is_not_used(self):
		pass
