from flask import Response, render_template, jsonify, request
from src.dao.user_dao import UserDao
from src.utils.User_utils import UserUtils

class User_deal():
	def get_names(self):
		return UserDao().get_users()

	def registerDeal_employe(self,data):
		tipo=''


		for i in data['tipoEmpleado']:
			tipo = i
		if tipo == 'Academico':
			data['tipo'] = data.pop('tipoCategoria')
			del data['tipoCategoriaAdmin']
		elif tipo == 'Administrativo':
			data['tipo'] = data.pop('tipoCategoriaAdmin')
			del data['tipoCategoria']
		lista = []
		for k,v in data.items():
			for i in v:
				lista.append(i)
		resu = UserDao().register_user(lista)
		print(type(resu))
		print("el resultado es ", str(resu))
		return render_template("AgregarEmpleado.html",estatus=str(resu))


	def get_shedule(self,data):

		return render_template('AgregarHorario.html',message=UserUtils().get_files(data))



	def findDeal_employee(self, data):
		result = UserDao().findDao_user(data)
		if result is False:
			mensaje = 'No se encontraron resultados'
			return render_template("verEmpleado.html",message= mensaje)
		else:
			return render_template("verEmpleado.html", result = result)

	def find_edit_employe(self,id):
		lista = []
		lista.append(id)
		dic = {
			'NoEmpleado': lista
		}
		print("estoy apunto de entrar a dao ", dic)
		result = UserDao().findDao_user(dic)
		return  result



	def update_registers(self,data):
		tipo=''
		for i in data['tipoEmpleado']:
			tipo = i
		if tipo == 'Academico':
			data['tipo'] = data.pop('tipoCategoria')
			del data['tipoCategoriaAdmin']
		elif tipo == 'Administrativo':
			data['tipo'] = data.pop('tipoCategoriaAdmin')
			del data['tipoCategoria']
		lista = []
		for k,v in data.items():
			for i in v:
				lista.append(i)
		resu = UserDao().register_update(lista)
		print(resu)
		print("el resultado es ", resu)
		return render_template("verEmpleado.html",message=resu)

	def del_registers(self,data):
		res = UserDao().del_user_dao(data)
		if res:
			return res
		else:
			mensaje = 'Ocurrio un error inesperado, intente mas tarde'
			return mensaje




