from os import walk
import os
import pandas as pd
from src.dao.user_dao import UserDao
import datetime

from openpyxl import load_workbook
class UserUtils:


	def create_files(self,lista,name):
		f = open('outputs/'+name+'_'+str(datetime.date.today())+'.csv', 'w')
		values = '\n'.join(str(v) for v in lista)
		f.write(values.replace('[','').replace(']',''.replace("'","").strip()))
		f.close()

	def get_files(self,data):
		PROJECT_HOME = os.path.dirname(os.path.realpath(__file__))
		UPLOAD_FOLDER = '{}/static/horarios'.format(PROJECT_HOME)
		archivo = 'static/horarios/'+data
		work = load_workbook(archivo,read_only=True)
		seet = work['Hoja1']
		lista = []
		for i in seet.iter_rows():
			temp = []
			for j in i:
				temp.append(j.value)
			lista.append(temp)
		coincidencias = []

		for i in lista:
			if str(i[1]) != 'None':
				result = UserDao().validate_usr(i[1])
				if result is True or str(result)=='True':
					esta = UserDao().register_schedule(i)
		coincidencias.append(esta)
		return coincidencias
