
function cancelaCurpGEM(){
	
	$('#esperar').show();

	var v_folio=document.getElementById("folio").value;	
	//alert(v_alumno);
	$.ajax("wsGEM5.php", { //localhost
		"type": "post",  
		"dataType" : "html",
		"data": { 
			folio: v_folio
		} 
	}).
	done(function(data) {
		$('#esperar').hide();
		$('#resultados').html(data);
		$('#resultados').show();
	})	
	.fail(function(result) {
		alert("Fallo");
	}); 
	
	
}
function buscarCurp(){
	
	$('#esperar').show();

	var v_curp=document.getElementById("curp").value;	
	//alert(v_alumno);
	$.ajax("wsGEM2.php", { //localhost
		"type": "post",  
		"dataType" : "html",
		"data": { 
			curp: v_curp
		} 
	}).
	done(function(data) {
		$('#esperar').hide();
		$('#resultados').html(data);
		$('#resultados').show();
	})	
	.fail(function(result) {
		alert("Fallo");
	}); 
	
	
}

function buscarAlumnoUAEM(){
	
	$('#esperar').show();

	var v_cuenta=document.getElementById("cuenta").value;	
	//alert(v_alumno);
	$.ajax("wsAlumnoSicde.php", { //localhost
		"type": "post",  
		"dataType" : "html",
		"data": { 
			cuenta: v_cuenta
		} 
	}).
	done(function(data) {
		$('#esperar').hide();
		$('#resultados').html(data);
		$('#resultados').show();
	})	
	.fail(function(result) {
		alert("Fallo");
	}); 
	
	
}
function buscarUsuarioUAEM(){
	
	$('#esperar').show();

	var v_usuario=document.getElementById("usuario").value;	
	var v_password=document.getElementById("password").value;	
	//alert(v_alumno);
	$.ajax("wsAlumnoSicde.php", { //localhost
		"type": "post",  
		"dataType" : "html",
		"data": { 
			cuenta: v_cuenta,
			password:v_password
		} 
	}).
	done(function(data) {
		$('#esperar').hide();
		$('#resultados').html(data);
		$('#resultados').show();
	})	
	.fail(function(result) {
		alert("Fallo");
	}); 
	
	
}


function buscarCurpGEM(){
	
	$('#esperar').show();

	var v_curp=document.getElementById("curp").value;	
	//alert(v_alumno);
	$.ajax("wsGEM3.php", { //localhost
		"type": "post",  
		"dataType" : "html",
		"data": { 
			curp: v_curp
		} 
	}).
	done(function(data) {
		$('#esperar').hide();
		$('#resultados').html(data);
		$('#resultados').show();
	})	
	.fail(function(result) {
		alert("Fallo");
	}); 
	
	
}
function cargarArchivo(){
	$('#esperar').show();
	if($("#fileElem").val().length!==0 ){
	var formData = new FormData();
	formData.append('fileElem2', $('input[type=file]')[0].files[0]); 
    formData.append('fecha',$("#fecha").val()); 
	$.ajax({
		url: 'referenciasbuzon.php',
		data: formData,
		type: "POST",
		contentType: false,
		processData: false,
	}).
	done(function(data) {
		$('#esperar').hide();
		$('#resultados').html(data);
		$('#resultados').show();
	})	
	.fail(function(result) {
		alert("Fallo");
	}); 
	
	}else{
		alert("No hay archivo seleccionado");
		}
	
}

function handleFiles(files) {
      var d = document.getElementById("fileList");
	  
	  if (!files.length) {
        d.innerHTML = "<p>No hay archivo seleccionados!</p>";
      } else {
      
		d.innerHTML="";
		var list = document.createElement("div");
        d.appendChild(list);
      
          var img = document.createElement("img");
          img.src = window.URL.createObjectURL(files[0]);;
          img.height = 80;
          img.onload = function() {
            window.URL.revokeObjectURL(this.src);
          }
          list.appendChild(img);
          
          var info = document.createElement("span");
          info.innerHTML = files[0].name ;
          list.appendChild(info);
		
		
	  }
	 
    }	


function mostrarC(){
	$('#esperar').show();

	var v_alumno=document.getElementById("alumno").value;	
	//alert(v_alumno);
	$.ajax("referenciasUAEM.php", { //localhost
		"type": "post",  
		"dataType" : "html",
		"data": { 
			alumno: v_alumno
		} 
	}).
	done(function(data) {
		$('#esperar').hide();
		$('#resultados').html(data);
		$('#resultados').show();
	})	
	.fail(function(result) {
		alert("Fallo");
	}); 
	
	
}

function mostrarR(){
	$('#esperar').show();

	var v_ref=document.getElementById("ref1").value;	
	//alert(v_alumno);
	$.ajax("mostrarReferencia.php", { //localhost
		"type": "post",  
		"dataType" : "html",
		"data": { 
			ref: v_ref
		} 
	}).
	done(function(data) {
		$('#esperar').hide();
		$('#resultados').html(data);
		$('#resultados').show();
	})	
	.fail(function(result) {
		alert("Fallo");
	}); 
	
	
}

function mostrarDes(){
	$('#esperar').show();

	var v_ref=document.getElementById("ref1").value;	
	//alert(v_alumno);
	$.ajax("mostrarReferenciaDes.php", { //localhost
		"type": "post",  
		"dataType" : "html",
		"data": { 
			ref: v_ref
		} 
	}).
	done(function(data) {
		$('#esperar').hide();
		$('#resultados').html(data);
		$('#resultados').show();
	})	
	.fail(function(result) {
		alert("Fallo");
	}); 
	
	
}


function mostrarA(){
	$('#esperar').show();


	var v_ref=document.getElementById("ref2").value;	
	var v_fecha=document.getElementById("fecha").value;	
	var v_sucursal=document.getElementById("sucursal").value;	
	var v_importe=document.getElementById("importe").value;	
	//alert(v_alumno);
	$.ajax("mostrarReferencia2.php", { //localhost
		"type": "post",  
		"dataType" : "html",
		"data": { 
			ref: v_ref, fecha: v_fecha,sucursal: v_sucursal,importe: v_importe
		} 
	}).
	done(function(data) {
		$('#esperar').hide();
		$('#resultados').html(data);
		$('#resultados').show();
	})	
	.fail(function(result) {
		alert("Fallo");
	}); 
	
	
}